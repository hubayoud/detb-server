const NOTIFICATION_TEST = {
	"notification": {
		"title": "Welcome on my app",
		"body": "You're subscribed to DETB notifications..."
	}
};

module.exports = NOTIFICATION_TEST;