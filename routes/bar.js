const router = require('express').Router();
const Bar = require('../models/bar.model');

// GET : /v1/bars
router.get('/', (req, res) => {
	Bar.find({}, (err, bars) => {
		if (err) {
			res.status(500).json(`Failed to fetch all bars`);
		} else {
			res.status(200).json(bars);
		}
	});
});

module.exports = router;