const Sub = require('../models/sub.model');
const router = require('express').Router();
const webPush = require('web-push');
const NOTIFICATION_TEST = require('../notification-test');


// Configurer la lib Web Push pour envoyer des notifications aux subs
const VAPIDKeys = {
	publicKey:"BClcIn39PpLhEUqQMR-6fvOWyxLfoLr2evEhKg9D9U8HnjKGR9e9bd-uXMBVra1WeenNn0Aa9unlngRCe90Kae0",
	privateKey:"-XuvKheqzA4YxmwlfIFuGHokd1yxazzJik3YWpcCuqs"
}

webPush.setVapidDetails('mailto:test@gmail.com', VAPIDKeys.publicKey, VAPIDKeys.privateKey);


// POST : /v1/notifications
router.post('/', (req, res) => {
	const sub = req.body;

	if (sub) {
		// definir le nouveau sub avant de l'envoyé
		const newSub = new Sub({
			details: sub
		});

		// sauvegarde dans la base de données
		newSub.save((err) => {
			if (err) { return res.status(500).json('save sub failed'); }
			res.json('sub successfully saved');
		});
	} else {
		res.status(404).json('sub not found');
	}
});


// GET : /v1/notifications/test
router.get('/test', (req, res) => {
	// Récupérer l'ensemble des sub en base et leur envoyer une notification
	Sub.find({}).exec().then((subs) => {
		Promise.all(subs.map((sub) => {
			webPush.sendNotification(sub.details, JSON.stringify(NOTIFICATION_TEST))
		})).then(() => {
			res.status(200).json(`Notifications successfully sent.`);
		}).catch(() => {
			res.status(500).json(`Failed to send notifications to web push service.`);
		});
	});
});

module.exports = router;