const router = require('express').Router();
const fs = require('fs');
const RSA_PUBLIC_KEY = fs.readFileSync('./rsa/id_rsa.pub');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');

// GET: /v1/user/current
router.get('/current', isLoggedIn ,(req, res) => {
	res.json(req.user);
});

function isLoggedIn(req, res, next) {
	const token = req.headers.authorization;

	if (token) {
		jwt.verify(token, RSA_PUBLIC_KEY, (err, decoded) => {
			if (err) { return res.status(401).json('token invalide !'); }

			const sub = decoded.sub;
			User.findOne({ '_id': sub }).exec((err, user) => {
				if (err || !user) { res.status(401).json('token valide mais erreur lors de la récupération du user'); }
				
				req.user = user;
				next();
			});
		});
	} else {
		res.status(401).json(`pas de token alors que vous essayer d'accéder à une ressource protégée`);
	}
}


module.exports = router;