const router = require('express').Router();
const auth = require('./auth');
const user = require('./user');
const bar = require('./bar');
const notifications = require('./notifications');
const menu = require('./menu');

router.use('/auth', auth);
router.use('/user', user);
router.use('/bars', bar);
router.use('/notifications', notifications);
router.use('/menu', menu);

// GET : /v1
router.get('/', (req, res, next) => {
	return res.status(200).json({ message: 'welcome message'});
});

module.exports = router;