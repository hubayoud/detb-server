const router = require('express').Router();
const Bar = require('../models/bar.model');
const Drink = require('../models/drink.model');

// GET : /v1/menu/:idBar
router.get('/:id', (req, res) => {
	Bar.findOne({ '_id': req.params.id }, (err, bar) => {
		if (err) {
			res.status(500).json(`Failed to fetch all bars`);
		} else {
			// on a récupéré un bar ici, on fetch toutes les boissons
			Drink.find({}, (err, drinks) => {
				if (err) {
					res.status(400).json(`bar fetched but error with drinks`);
				} else {
					const menu = {
						bar: bar,
						drinks: drinks
					};

					res.status(200).json(menu);
				}
			})
		}
	});
});

module.exports = router;