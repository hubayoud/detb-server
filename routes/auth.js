const auth = require('express').Router();
const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const RSA_PUBLIC_KEY = fs.readFileSync('./rsa/id_rsa.pub');
const RSA_PRIVATE_KEY = fs.readFileSync('./rsa/id_rsa'); 


// POST : /v1/auth/signin
auth.post('/signin', (req, res) => {
	// fonction asynchrone
	User.findOne({ 'email': req.body.email }).exec((err, user) => {
		// si il y a un user et si le password rentré est correct
		if (user && bcrypt.compareSync(req.body.password, user.password)) {
			// génération du token
			const token = jwt.sign({}, RSA_PRIVATE_KEY, {
				algorithm: 'RS256',
				expiresIn: '15s',
				subject: user._id.toString()
			});
			res.status(200).json(token);
		} else {
			res.status(401).json(`signin failed with error : ${err}`);
		}
	});
});


// POST : /v1/auth/signup
auth.post('/signup', (req, res) => {
	const newUser = new User({
		email: req.body.email,
		pseudo: req.body.pseudo,
		password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8)),
		consentCGU: req.body.consentCGU
	});

	newUser.save((err) => {
		if (err) { res.status(500).json('error in signup') };
		res.status(200).json('signup ok');
	});
});


// GET : /v1/auth/refresh_token
auth.get('/refresh_token', (req, res) => {
	const token = req.headers.authorization;

	if (token) {
		jwt.verify(token, RSA_PUBLIC_KEY, (err, decoded) => {
			if (err) { return res.status(403).json('wrong token'); }

			const newToken = jwt.sign({}, RSA_PRIVATE_KEY, {
				algorithm: 'RS256',
				expiresIn: '15s',
				subject: decoded.sub
			});

			res.status(200).json(newToken);
		});
	} else {
		res.json(403).json('no token !');
	}
});

module.exports = auth;