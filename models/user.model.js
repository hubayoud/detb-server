const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = Schema({
	email: String,
	pseudo: String,
	password: String,
	consentCGU: Boolean,
});

const User = mongoose.model('User', userSchema);

module.exports = User;