const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const drinkSchema = Schema({
	id: Number,
	shortname: String,
	longname: String,
	description: String,
	idFamilyDrink: Number,
	abv: String,
	img: String,
	sizes: [Number],
	adress: String,
	logo: String,
});

const Drink = mongoose.model('Drink', drinkSchema);

module.exports = Drink;