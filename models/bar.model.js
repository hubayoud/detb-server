const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const barSchema = Schema({
	shortname: String,
	longname: String,
	description: String,
	adress: String,
	logo: String,
});

const Bar = mongoose.model('Bar', barSchema);

module.exports = Bar;